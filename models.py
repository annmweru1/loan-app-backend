from django.db import models
from django.db.models.fields import EmailField

# Create your models here.

class personal_particulars(models.Model):
        Membership_No= models.CharField(max_length=30)
        First_name = models.CharField(max_length=30)
        Middle_Name= models.CharField(max_length=30)
        Last_name = models.CharField(max_length=30)
        id_no_passport_no= models.CharField(max_length=30)
        dob = models.CharField(max_length=30)
        Home_address = models.CharField(max_length=30)
        Office_Tel_No= models.CharField(max_length=30)
        Mobile_No = models.CharField(max_length=30)
        Pin_No= models.CharField(max_length=30)
        EmailField = models.CharField(max_length=30)
        marital_status=Single= models.CharField(max_length=30)
        No_Of_dependents= models.CharField(max_length=30)


class pysical_address(models.Model):
        town= models.CharField(max_length=30)
        estate = models.CharField(max_length=30)
        street_house_no= models.CharField(max_length=30)
        how_long_have_you_lived_there_year_months = models.CharField(max_length=30)
        rented_owned= models.CharField(max_length=30)
        dob = models.CharField(max_length=30)

        


       
        

class Employment_details(models.Model):
        Applicant_employee= models.CharField(max_length=30)
        postal_Address = models.CharField(max_length=30)
        physical_address= models.CharField(max_length=30)
        Designated = models.CharField(max_length=30)
        Tel= models.CharField(max_length=30)
        staff_number= models.CharField(max_length=30)
        
class Business_details(models.Model):
        Business_income= models.CharField(max_length=30)
        Rental_income = models.CharField(max_length=30)
        other_income= models.CharField(max_length=30)
        

class Bank_details(models.Model):
        Account_Name= models.CharField(max_length=30)
        Account_No = models.CharField(max_length=30)
        Bank= models.CharField(max_length=30)
        Branch= models.CharField(max_length=30)
        Bank_code= models.CharField(max_length=30)
        Bank_code_= models.CharField(max_length=30)
       
        

class Loan_particulars(models.Model):
        Loan_type= models.CharField(max_length=30)
        Normal_loan = models.CharField(max_length=30)
        Developement_loan= models.CharField(max_length=30)
        Emergency_school_fees_loan = models.CharField(max_length=30)
        

class Loan_in_other_banks(models.Model):
        No= models.CharField(max_length=30)
        Name_of_the_Bank_Institution = models.CharField(max_length=30)
        amount_advanced= models.CharField(max_length=30)
        Date_granted = models.CharField(max_length=30)
        Repayment_period= models.CharField(max_length=30)
        Outstanding_balance = models.CharField(max_length=30)
        
                              


