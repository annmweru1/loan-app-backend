from django.contrib import admin

from .models import PersonalParticulars, PhysicalAddress, EmploymentDetails, BusinessDetails, LoanParticulars, LoansInOtherBanks, BankDetails

# Register your models here.

admin.site.register(PersonalParticulars)
admin.site.register(PhysicalAddress)
admin.site.register(EmploymentDetails)
admin.site.register(BusinessDetails)
admin.site.register(LoanParticulars)
admin.site.register(LoansInOtherBanks)
admin.site.register(BankDetails)
