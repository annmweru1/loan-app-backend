from django.urls import include, path
from django.conf.urls import url
from rest_framework import routers, urlpatterns

from .views import personal_list, personal_detail, personal_list_published, physical_list, physical_detail, physical_list_published, employment_list, employment_detail, employment_list_published, business_list, business_detail, business_list_published, loanparticula_list, loanparticular_detail, loanparticular_list_published, loanotherbank_list, loanotherbank_detail, loanotherbank_list_published, bank_detail, bankdetails_list, bankdetail_list_published

urlpatterns = [
    url(r'^api/personal$', personal_list),
    url(r'^api/personal/(?P<pk>[0-9]+)$', personal_detail),
    url(r'^api/personal/published$', personal_list_published),
    url(r'^api/physicaladdress$', physical_list),
    url(r'^api/physicaladdress/(?P<pk>[0-9]+)$', physical_detail),
    url(r'^api/physicaladdress/published$', physical_list_published),
    url(r'^api/employment$', employment_list),
    url(r'^api/employment/(?P<pk>[0-9]+)$', employment_detail),
    url(r'^api/employment/published$', employment_list_published),
    url(r'^api/business$', business_list),
    url(r'^api/business/(?P<pk>[0-9]+)$', business_detail),
    url(r'^api/business/published$', business_list_published),
    url(r'^api/loanpart$', loanparticula_list),
    url(r'^api/loanpart/(?P<pk>[0-9]+)$', loanparticular_detail),
    url(r'^api/loanpart/published$', loanparticular_list_published),
    url(r'^api/loanotherbank$', loanotherbank_list),
    url(r'^api/loanotherbank/(?P<pk>[0-9]+)$', loanotherbank_detail),
    url(r'^api/loanotherbank/published$', loanotherbank_list_published),
    url(r'^api/bankdetail$', bankdetails_list),
    url(r'^api/bankdetail/(?P<pk>[0-9]+)$', bank_detail),
    url(r'^api/loanotherbank/published$', bankdetail_list_published)
]