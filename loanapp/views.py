from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework import viewsets

from .serializers import PersonalSerializer, PhysicalAddressSerializer, EmploymentDetailsSerializer, BusinessDetailsSerializer, LoanParticularsSerializer, LoansInOtherBanksSerializer, BankDetailsSerializer
from .models import PersonalParticulars, PhysicalAddress, EmploymentDetails, BusinessDetails, LoanParticulars, LoansInOtherBanks, BankDetails

@api_view(['GET', 'POST', 'DELETE'])
def personal_list(request):
    if request.method == 'GET':
        queryset = PersonalParticulars.objects.all()
        personal_serializer = PersonalSerializer(queryset, many=True)
        return JsonResponse(personal_serializer.data, safe=False)
    elif request.method == 'POST':
        personal_data = JSONParser().parse(request)
        personal_serializer = PersonalSerializer(data=personal_data)
        if personal_serializer.is_valid():
            new_person = personal_serializer.save()
            tmp = {
                "id" : new_person.pk,
                "data" : personal_serializer.data,
            }
            return JsonResponse(tmp, status=status.HTTP_201_CREATED)
        return JsonResponse(personal_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        personal_count = PersonalParticulars.objects.all()
        return JsonResponse({'message':'{} personal data were deleted successfully!'.format(personal_count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def personal_detail(request, pk):
    try:
        personal = PersonalParticulars.objects.get(pk=pk)
    except PersonalParticulars.DoesNotExist:
        return JsonResponse({'message': 'The personal data does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        personal_serializer = PersonalSerializer(personal)
        return JsonResponse(personal_serializer.data)

    elif request.method == 'PUT':
        personal_data = JSONParser().parse(request)
        personal_serializer = PersonalSerializer(personal, data=personal_data)
        if personal_serializer.is_valid():
            personal_serializer.save()
            return JsonResponse(personal_serializer.data)
        return JsonResponse(personal_serializer.errors, status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        personal.delete()
        return JsonResponse({'message': 'Personal data was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def personal_list_published(request):
    personal = PersonalParticulars.objects.filter(published=True)

    if request.method == 'GET':
        personal_serializer = PersonalSerializer(personal, many=True)
        return JsonResponse(personal_serializer.data, safe=False)

@api_view(['GET', 'POST', 'DELETE'])
def physical_list(request):
    if request.method == 'GET':
        queryset = PhysicalAddress.objects.all()
        physical_serializer = PhysicalAddressSerializer(queryset, many=True)
        return JsonResponse(physical_serializer.data, safe=False)

    elif request.method == 'POST':
        physical_data = JSONParser().parse(request)
        physical_serializer = PhysicalAddressSerializer(data=physical_data)
        if physical_serializer.is_valid():
            physical_serializer.save()
            return JsonResponse(physical_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(physical_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        physical_count = PhysicalAddress.objects.all()
        return JsonResponse({'message':'{} physical address data were deleted successfully!'.format(physical_count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def physical_detail(request, pk):
    try:
        physical_query = PhysicalAddress.objects.get(pk=pk)
    except PhysicalAddress.DoesNotExist:
        return JsonResponse({'message': 'The physical address does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        physicaladdress_serializer = PhysicalAddressSerializer(physical_query)
        return JsonResponse(physicaladdress_serializer.data)

    elif request.method == 'PUT':
        physicaladdress_data = JSONParser.parse(request)
        physicaladdress_serializer = PhysicalAddressSerializer(physical_query, data=physicaladdress_data)
        if physicaladdress_serializer.is_valid():
            physicaladdress_serializer.save()
            return JsonResponse(physicaladdress_serializer.data)
        return JsonResponse(physicaladdress_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def physical_list_published(request):
    physicaladdress_query = PhysicalAddress.objects.filter(published=True)

    if request.method == 'GET':
        physicaladdress_serializer = PhysicalAddressSerializer(physicaladdress_query, many=True)
        return JsonResponse(physicaladdress_serializer.data, safe=False)

@api_view(['GET', 'POST', 'DELETE'])
def employment_list(request):
    if request.method == 'GET':
        employment_query = EmploymentDetails.objects.all()
        employment_serializer = EmploymentDetailsSerializer(employment_query, many=True)
        return JsonResponse(employment_serializer.data, safe=False)

    elif request.method == 'POST':
        employment_data = JSONParser().parse(request)
        employment_serializer = EmploymentDetailsSerializer(data=employment_data)
        if employment_serializer.is_valid():
            employment_serializer.save()
            return JsonResponse(employment_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(employment_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        employment_count = EmploymentDetails.objects.all()
        return JsonResponse({'message':'{} employment data were deleted successfully!'.format(employment_count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def employment_detail(request, pk):
    try:
        employment_query = EmploymentDetails.objects.get(pk=pk)
    except EmploymentDetails.DoesNotExist:
        return JsonResponse({'message': 'The employment data does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        employment_serializer = PhysicalAddressSerializer(employment_query)
        return JsonResponse(employment_serializer.data)

    elif request.method == 'PUT':
        employment_data = JSONParser.parse(request)
        employment_serializer = PhysicalAddressSerializer(employment_query, data=employment_data)
        if employment_serializer.is_valid():
            employment_serializer.save()
            return JsonResponse(employment_serializer.data)
        return JsonResponse(employment_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def employment_list_published(request):
    employment_query = EmploymentDetails.objects.filter(published=True)

    if request.method == 'GET':
        employment_serializer = EmploymentDetailsSerializer(employment_query, many=True)
        return JsonResponse(employment_serializer.data, safe=False)

@api_view(['GET', 'POST', 'DELETE'])
def business_list(request):
    if request.method == 'GET':
        business_query = BusinessDetails.objects.all()
        business_serializer = BusinessDetails(business_query, many=True)
        return JsonResponse(business_serializer.data, safe=False)

    elif request.method == 'POST':
        business_data = JSONParser().parse(request)
        business_serializer = BusinessDetailsSerializer(data=business_data)
        if business_serializer.is_valid():
            business_serializer.save()
            return JsonResponse(business_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(business_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        business_count = BusinessDetails.objects.all()
        return JsonResponse({'message':'{} business data were deleted successfully!'.format(business_count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def business_detail(request, pk):
    try:
        business_query = BusinessDetails.objects.get(pk=pk)
    except BusinessDetails.DoesNotExist:
        return JsonResponse({'message': 'The business data does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        business_serializer = BusinessDetailsSerializer(business_query)
        return JsonResponse(business_serializer.data)

    elif request.method == 'PUT':
        business_data = JSONParser.parse(request)
        business_serializer = BusinessDetailsSerializer(business_query, data=business_data)
        if business_serializer.is_valid():
            business_serializer.save()
            return JsonResponse(business_serializer.data)
        return JsonResponse(business_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def business_list_published(request):
    business_query = BusinessDetails.objects.filter(published=True)

    if request.method == 'GET':
        business_serializer = BusinessDetailsSerializer(business_query, many=True)
        return JsonResponse(business_serializer.data, safe=False)

@api_view(['GET', 'POST', 'DELETE'])
def loanparticula_list(request):
    if request.method == 'GET':
        loanpart_query = LoanParticulars.objects.all()
        loanpart_serializer = LoanParticularsSerializer(loanpart_query, many=True)
        return JsonResponse(loanpart_serializer.data, safe=False)

    elif request.method == 'POST':
        loanpart_data = JSONParser().parse(request)
        loanpart_serializer = LoanParticularsSerializer(data=loanpart_data)
        if loanpart_serializer.is_valid():
            loanpart_serializer.save()
            return JsonResponse(loanpart_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(loanpart_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        loanpart_count = LoanParticulars.objects.all()
        return JsonResponse({'message':'{} loan particular data were deleted successfully!'.format(loanpart_count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def loanparticular_detail(request, pk):
    try:
        loanpart_query = LoanParticulars.objects.get(pk=pk)
    except LoanParticulars.DoesNotExist:
        return JsonResponse({'message': 'The loan particular data does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        loanpart_serializer = LoanParticularsSerializer(loanpart_query)
        return JsonResponse(loanpart_serializer.data)

    elif request.method == 'PUT':
        loanpart_data = JSONParser.parse(request)
        loanpart_serializer = BusinessDetailsSerializer(loanpart_query, data=loanpart_data)
        if loanpart_serializer.is_valid():
            loanpart_serializer.save()
            return JsonResponse(loanpart_serializer.data)
        return JsonResponse(loanpart_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def loanparticular_list_published(request):
    loanpart_query = LoanParticulars.objects.filter(published=True)

    if request.method == 'GET':
        loanpart_serializer = LoanParticularsSerializer(loanpart_query, many=True)
        return JsonResponse(loanpart_serializer.data, safe=False)

@api_view(['GET', 'POST', 'DELETE'])
def loanotherbank_list(request):
    if request.method == 'GET':
        loanotherbanks_query = LoansInOtherBanks.objects.all()
        loanotherbanks_serializer = LoansInOtherBanksSerializer(loanotherbanks_query, many=True)
        return JsonResponse(loanotherbanks_serializer.data, safe=False)

    elif request.method == 'POST':
        loanotherbank_data = JSONParser().parse(request)
        loanotherbanks_serializer = LoansInOtherBanksSerializer(data=loanotherbank_data)
        if loanotherbanks_serializer.is_valid():
            loanotherbanks_serializer.save()
            return JsonResponse(loanotherbanks_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(loanotherbanks_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        loanotherbanks_count = LoansInOtherBanks.objects.all()
        return JsonResponse({'message':'{} loan on other bank data were deleted successfully!'.format(loanotherbanks_count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def loanotherbank_detail(request, pk):
    try:
        loanotherbank_query = LoansInOtherBanks.objects.get(pk=pk)
    except LoansInOtherBanks.DoesNotExist:
        return JsonResponse({'message': 'The loan on other banks data does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        loanotherbanks_serializer = LoansInOtherBanksSerializer(loanotherbank_query)
        return JsonResponse(loanotherbanks_serializer.data)

    elif request.method == 'PUT':
        loanotherbank_data = JSONParser.parse(request)
        loanotherbank_serializer = LoansInOtherBanksSerializer(loanotherbank_query, data=loanotherbank_data)
        if loanotherbank_serializer.is_valid():
            loanotherbank_serializer.save()
            return JsonResponse(loanotherbank_serializer.data)
        return JsonResponse(loanotherbank_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def loanotherbank_list_published(request):
    loanotherbanks_query = LoansInOtherBanks.objects.filter(published=True)

    if request.method == 'GET':
        loanotherbanks_serializer = LoanParticularsSerializer(loanotherbanks_query, many=True)
        return JsonResponse(loanotherbanks_serializer.data, safe=False)

@api_view(['GET', 'POST', 'DELETE'])
def bankdetails_list(request):
    if request.method == 'GET':
        bankdetails_query = BankDetails.objects.all()
        bankdetails_serializer = BankDetailsSerializer(bankdetails_query, many=True)
        return JsonResponse(bankdetails_serializer.data, safe=False)

    elif request.method == 'POST':
        bankdetails_data = JSONParser().parse(request)
        bankdetails_serializer = BankDetailsSerializer(data=bankdetails_data)
        if bankdetails_serializer.is_valid():
            bankdetails_serializer.save()
            return JsonResponse(bankdetails_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(bankdetails_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        bankdetails_count = BankDetails.objects.all()
        return JsonResponse({'message':'{} bank detail data were deleted successfully!'.format(bankdetails_count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def bank_detail(request, pk):
    try:
        bankdetail_query = BankDetails.objects.get(pk=pk)
    except BankDetails.DoesNotExist:
        return JsonResponse({'message': 'Bank detail data does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        bankdetail_serializer = BankDetailsSerializer(bankdetail_query)
        return JsonResponse(bankdetail_serializer.data)

    elif request.method == 'PUT':
        bankdetail_data = JSONParser.parse(request)
        bankdetail_serializer = BankDetailsSerializer(bankdetail_query, data=bankdetail_data)
        if bankdetail_serializer.is_valid():
            bankdetail_serializer.save()
            return JsonResponse(bankdetail_serializer.data)
        return JsonResponse(bankdetail_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def bankdetail_list_published(request):
    bankdetail_query = BankDetails.objects.filter(published=True)

    if request.method == 'GET':
        bankdetail_serializer = BankDetailsSerializer(bankdetail_query, many=True)
        return JsonResponse(bankdetail_serializer.data, safe=False) 

"""
# Create your views here.
class PersonalViewSet(viewsets.ModelViewSet):
    queryset = PersonalParticulars.objects.all()
    serializer_class = PersonalSerializer

class PhysicalAddressViewSet(viewsets.ModelViewSet):
    queryset = PhysicalAddress.objects.all()
    serializer_class = PhysicalAddressSerializer

class EmploymentViewSet(viewsets.ModelViewSet):
    queryset = EmploymentDetails.objects.all()
    serializer_class = EmploymentDetailsSerializer

class BusinessViewSet(viewsets.ModelViewSet):
    queryset = BusinessDetails.objects.all()
    serializer_class = BusinessDetailsSerializer

class LoanParticularsViewSet(viewsets.ModelViewSet):
    queryset = LoanParticulars.objects.all()
    serializer_class = LoanParticularsSerializer

class LoansInOtherBanksViewSet(viewsets.ModelViewSet):
    queryset = LoansInOtherBanks.objects.all()
    serializer_class = LoansInOtherBanksSerializer
"""