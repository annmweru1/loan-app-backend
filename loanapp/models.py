from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from djmoney.models.fields import MoneyField

MARITAL_STATUS_CHOICES = [
    ('Single', 'Single'),
    ('Married', 'Married'),
    ('Widowed', 'Widowed'),
    ('Others', 'Others')
]

EMPLOYMENT_TERMS_CHOICES = [
    ('Permanent', 'Permanent'),
    ('Casual', 'Casual'),
    ('Contract', 'Contract'),
    ('Others', 'Others')
]

LOAN_TYPE_CHOICES = [
    ('Normal loan', 'Normal loan'),
    ('Development loan', 'Development loan'),
    ('Emergency/School loan', 'Emergency/School loan')
]


class PersonalParticulars(models.Model):
    membership_no = models.CharField(max_length=30)
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    id_no_passport_no = models.IntegerField(max_length=20)
    dob = models.DateField()
    home_address = models.CharField(max_length=50)
    office_tel_no =  models.CharField(max_length=12)
    mobile_no =  models.CharField(max_length=12)
    pin_no = models.IntegerField()
    email = models.EmailField()
    marital_status = models.CharField(max_length=15, choices=MARITAL_STATUS_CHOICES, default='Single')
    no_of_dependencies = models.IntegerField()


class PhysicalAddress(models.Model):
    town = models.CharField(max_length=50)
    estate = models.CharField(max_length=50)
    street_house_no = models.CharField(max_length=50)
    no_of_years_lived = models.IntegerField()
    no_of_months_lived = models.IntegerField()
    rented_or_owned = models.BooleanField() 
    personal_particulars = models.ForeignKey(PersonalParticulars,on_delete=models.CASCADE, null=True, blank=True)

class EmploymentDetails(models.Model):
    applicants_employer = models.CharField(max_length=50)
    postal_address = models.CharField(max_length=50)
    physical_address = models.CharField(max_length=70)
    designated = models.CharField(max_length=50)
    tel_phone = PhoneNumberField()
    staff_number = models.IntegerField()
    
    employment_terms = models.CharField(max_length=20, choices=EMPLOYMENT_TERMS_CHOICES, default='')
    personal_particulars = models.ForeignKey(PersonalParticulars,on_delete=models.CASCADE, null=True, blank=True)

class BusinessDetails(models.Model):
    business_income = MoneyField(max_digits=14, decimal_places=2, default_currency=None)
    rental_income = MoneyField(max_digits=24, decimal_places=2, default_currency=None)
    other_income = MoneyField(max_digits=14, decimal_places=2, default_currency=None)
    personal_particulars = models.ForeignKey(PersonalParticulars,on_delete=models.CASCADE, null=True, blank=True)

class BankDetails(models.Model):
    account_name = models.CharField(max_length=50)
    account_no = models.CharField(max_length=50)
    bank = models.CharField(max_length=50)
    branch = models.CharField(max_length=50)
    branch_code = models.CharField(max_length=30)
    bank_code = models.CharField(max_length=30)
    personal_particulars = models.ForeignKey(PersonalParticulars,on_delete=models.CASCADE, null=True, blank=True)

class LoanParticulars(models.Model):
    loan_type = models.CharField(max_length=100, choices=LOAN_TYPE_CHOICES, default='Normal loan')
    purpose_of_loan = models.TextField(max_length=300)
    amount_applied = MoneyField(max_digits=14, decimal_places=2, default_currency=None)
    personal_particulars = models.ForeignKey(PersonalParticulars,on_delete=models.CASCADE, null=True, blank=True)

class LoansInOtherBanks(models.Model):
    name_of_institution = models.CharField(max_length=50)
    amount_advanced = MoneyField(max_digits=14, decimal_places=2, default_currency=None)
    date_granted = models.DateField()
    repayment_period = models.IntegerField()
    outstanding_balance = MoneyField(max_digits=14, decimal_places=2, default_currency=None)
    personal_particulars = models.ForeignKey(PersonalParticulars,on_delete=models.CASCADE, null=True, blank=True)