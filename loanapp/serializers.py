from rest_framework import serializers

from .models import PersonalParticulars, PhysicalAddress, EmploymentDetails, BusinessDetails, LoanParticulars, LoansInOtherBanks, BankDetails

class PersonalSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalParticulars
        fields = ('membership_no', 'first_name', 'middle_name', 'last_name', 'id_no_passport_no', 'dob', 'home_address', 'office_tel_no', 'mobile_no', 'pin_no', 'email', 'marital_status', 'no_of_dependencies')

class PhysicalAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhysicalAddress
        fields = ('town', 'estate', 'street_house_no', 'no_of_years_lived', 'no_of_months_lived', 'rented_or_owned', 'personal_particulars','personal_particulars')


class EmploymentDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmploymentDetails
        fields = ('applicants_employer', 'postal_address', 'physical_address', 'designated', 'tel_phone', 'staff_number', 'employment_terms','personal_particulars')

class BusinessDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessDetails
        fields = ('business_income', 'rental_income', 'other_income','personal_particulars')

class LoanParticularsSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoanParticulars
        fields = ('loan_type', 'purpose_of_loan', 'amount_applied','personal_particulars')

class LoansInOtherBanksSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoansInOtherBanks
        fields = ('name_of_institution', 'amount_advanced', 'date_granted', 'repayment_period', 'outstanding_balance', 'personal_particulars')

class BankDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankDetails
        fields = "__all__"
